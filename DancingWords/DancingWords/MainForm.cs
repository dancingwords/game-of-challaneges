﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
using VideoWriter;
using DancingWords;
using DancingWords.BusinessObjects;
using opentk2;
using System.IO;

namespace DancingWords
{
    public partial class MainForm : Form
    {
        private List<Typograph> typographies;
        //private EffectManager manager = new EffectManager(new VideoWriterAgent(Consts.outputVideoFileName, Consts.width, Consts.height, (int)(Consts.frameRate)));
        private int currentEffectSelectionIndex = 0;
        private List<string> effectNames = new List<string>();

        public MainForm()
        {
            InitializeComponent();
            ManageControls();
        }

        private void createVideoButton_Click(object sender, EventArgs e)
        {
            string videoPath = Path.ChangeExtension(this.audioFilePath.Text,"avi");
            EffectManager manager = new EffectManager(new VideoWriterAgent(videoPath, Consts.width, Consts.height, (int)(Consts.frameRate)));
            foreach (Typograph tp in typographies)
            {
                manager.Add(tp.effect);
            }

            using (var game = new opentk2.DancingWords(manager, Consts.width, Consts.height))
            {
                game.Run();
            }

            IAudioMixerAgent audioMixer = new AudioMixerAgent();
            audioMixer.AddAudio(this.audioFilePath.Text, videoPath);
            
            var res = MessageBox.Show("Video has been created successfully. Would you like to open it?", "Success", MessageBoxButtons.YesNo);
            if(res == DialogResult.Yes )
            {
                Process.Start( videoPath );
            }
        }

        private void selectAudioButton_Click(object sender, EventArgs e)
        {
            string filePath = SelectFileDialog("Audio Files (.mp3)|*.mp3");
            this.audioFilePath.Text = filePath;
        }

        private string SelectFileDialog(string fileTypeFilters)
        {
            string filePath = string.Empty;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = fileTypeFilters;
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;

            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == DialogResult.OK)
            {
                filePath = openFileDialog1.FileName;
            }

            return filePath;
        }

        private void selectSubtitleButton_Click(object sender, EventArgs e)
        {
            string filePath = SelectFileDialog("Subtitle Files (.srt)|*.srt");
            this.subtitlePath.Text = filePath;
        }

        private void loadSubtitleButton_Click(object sender, EventArgs e)
        {
            string filePath = subtitlePath.Text;
            if(filePath.Length < 1 && File.Exists(filePath))
            {
                MessageBox.Show("Please select correct file");
                return;
            }
            
            if (typographies != null && typographies.Count > 0)
            {
                typographies.Clear();
            }

            //Parse through the SRT file and create business objects from it
            typographies = SrtParser.ParseFile(filePath);
            if(typographies.Count < 1)
            {
                MessageBox.Show("Could not load data from SRT file.");
                return;
            }

            //initialize it to show second typograph. First is dedicated for partiles(big bang) effect
            currentEffectSelectionIndex = 2;

            PopulateEffectsComboBox();
            ShowTypographValues(currentEffectSelectionIndex - 1);
            ManageControls();
        }

        private void prevTypographButton_Click(object sender, EventArgs e)
        {
            currentEffectSelectionIndex = currentEffectSelectionIndex - 1;
            ShowTypographValues(currentEffectSelectionIndex - 1);
            ManageControls();
        }

        private void nextTypographButton_Click(object sender, EventArgs e)
        {
            currentEffectSelectionIndex = currentEffectSelectionIndex + 1;
            ShowTypographValues(currentEffectSelectionIndex - 1);
            ManageControls();
        }

        private void ShowTypographValues(int typographIndex)
        {
            Typograph tp = typographies[typographIndex];
            string effectName = tp.effect.GetType().Name;
            if (!this.effectsComboBox.Items.Contains(effectName))
            {
                currentEffectSelectionIndex = currentEffectSelectionIndex + 1;

                tp = typographies[typographIndex+1];
                effectName = tp.effect.GetType().Name;
            }

            this.startTimeValueLabel.Text = tp.StartTimeRaw;
            this.endTimeValueLabel.Text = tp.EndTimeRaw;
            this.lineValueLabel.Text = SrtParser.CreateStringFromList(tp.Lines);
            this.effectsComboBox.SelectedIndex = effectNames.FindIndex(x => x == effectName);
            //this.effectsComboBox.SelectedText = effectName;
        }

        private void ManageControls()
        {
            if (currentEffectSelectionIndex < 2)
            {
                this.prevTypographButton.Enabled = false;
                this.nextTypographButton.Enabled = false;
            }
            else if (currentEffectSelectionIndex == 2)
            {
                this.prevTypographButton.Enabled = false;
                this.nextTypographButton.Enabled = true;
            }
            else if (currentEffectSelectionIndex == typographies.Count)
            {
                this.prevTypographButton.Enabled = true;
                this.nextTypographButton.Enabled = false;
            }
            else
            {
                this.prevTypographButton.Enabled = true;
                this.nextTypographButton.Enabled = true;
            }
        }

        private void PopulateEffectsComboBox()
        {
            if (this.effectsComboBox.Items.Count < 1)
            {
                Array enumValues = Enum.GetValues(typeof(Consts.Effects));
                foreach (Consts.Effects e in enumValues)
                {
                    effectNames.Add(e.ToString());
                }

                this.effectsComboBox.DataSource = effectNames;
            }
        }

        private void saveEffectButton_Click(object sender, EventArgs e)
        {
            if (typographies.Count < 1)
            {
                return;
            }

            Typograph tp = typographies[currentEffectSelectionIndex-1];
            
            EffectOptions options = tp.effect.option;

            Consts.Effects eEffect = (Consts.Effects)Enum.Parse(typeof(Consts.Effects), this.effectsComboBox.Text);    
            //Consts.Effects eEffect = (Consts.Effects)int.Parse(this.effectsComboBox.Text);
            switch (eEffect)
            {
                case Consts.Effects.ColorChangeEffect:
                    tp.effect = new ColorChangeEffect(options);
                    break;
                case Consts.Effects.ZoomInEffect:
                    tp.effect = new ZoomInEffect(options);
                    break;
                case Consts.Effects.ZoomOutEffect:
                    tp.effect = new ZoomOutEffect(options);
                    break;
                case Consts.Effects.BounceEffect:
                    tp.effect = new BounceEffect(options);
                    break;
                case Consts.Effects.BounceLetterEffect:
                    tp.effect = new BounceLetterEffect(options);
                    break;
                case Consts.Effects.FadeInEffect:
                    options.color = Color.YellowGreen;
                    tp.effect = new FadeInEffect(options);
                    break;
                case Consts.Effects.FadeOutEffect:
                    options.color = Color.Red;
                    tp.effect = new FadeOutEffect(options);
                    break;
                case Consts.Effects.RandomLocationWordsEffect:
                    options.color = Color.Yellow;
                    tp.effect = new RandomLocationWordsEffect(options);
                    break;
                case Consts.Effects.ScrollTextEffect:
                    {
                        tp.effect = new ScrollTextEffect(options);
                        break;
                    }
                case Consts.Effects.TypingEffect:
                    tp.effect = new TypingEffect(options);
                    break;
                default:
                    break;
            }
        }
                
    }
}
