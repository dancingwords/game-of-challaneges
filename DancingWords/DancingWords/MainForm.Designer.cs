﻿namespace DancingWords
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createVideoButton = new System.Windows.Forms.Button();
            this.selectAudioButton = new System.Windows.Forms.Button();
            this.audioFilePath = new System.Windows.Forms.Label();
            this.selectSubtitleButton = new System.Windows.Forms.Button();
            this.subtitlePath = new System.Windows.Forms.Label();
            this.loadSubtitleButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.effectsComboBox = new System.Windows.Forms.ComboBox();
            this.effectLabel = new System.Windows.Forms.Label();
            this.lineValueLabel = new System.Windows.Forms.Label();
            this.endTimeValueLabel = new System.Windows.Forms.Label();
            this.startTimeValueLabel = new System.Windows.Forms.Label();
            this.linesLabel = new System.Windows.Forms.Label();
            this.nextTypographButton = new System.Windows.Forms.Button();
            this.prevTypographButton = new System.Windows.Forms.Button();
            this.endTimeLable = new System.Windows.Forms.Label();
            this.startTimeLabel = new System.Windows.Forms.Label();
            this.saveEffectButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // createVideoButton
            // 
            this.createVideoButton.Location = new System.Drawing.Point(32, 474);
            this.createVideoButton.Name = "createVideoButton";
            this.createVideoButton.Size = new System.Drawing.Size(136, 26);
            this.createVideoButton.TabIndex = 0;
            this.createVideoButton.Text = "Create Video";
            this.createVideoButton.UseVisualStyleBackColor = true;
            this.createVideoButton.Click += new System.EventHandler(this.createVideoButton_Click);
            // 
            // selectAudioButton
            // 
            this.selectAudioButton.Location = new System.Drawing.Point(33, 27);
            this.selectAudioButton.Name = "selectAudioButton";
            this.selectAudioButton.Size = new System.Drawing.Size(136, 23);
            this.selectAudioButton.TabIndex = 2;
            this.selectAudioButton.Text = "Select Audio File";
            this.selectAudioButton.UseVisualStyleBackColor = true;
            this.selectAudioButton.Click += new System.EventHandler(this.selectAudioButton_Click);
            // 
            // audioFilePath
            // 
            this.audioFilePath.AutoSize = true;
            this.audioFilePath.Location = new System.Drawing.Point(200, 32);
            this.audioFilePath.Name = "audioFilePath";
            this.audioFilePath.Size = new System.Drawing.Size(0, 17);
            this.audioFilePath.TabIndex = 3;
            // 
            // selectSubtitleButton
            // 
            this.selectSubtitleButton.Location = new System.Drawing.Point(32, 77);
            this.selectSubtitleButton.Name = "selectSubtitleButton";
            this.selectSubtitleButton.Size = new System.Drawing.Size(136, 23);
            this.selectSubtitleButton.TabIndex = 4;
            this.selectSubtitleButton.Text = "Select Subtitle File";
            this.selectSubtitleButton.UseVisualStyleBackColor = true;
            this.selectSubtitleButton.Click += new System.EventHandler(this.selectSubtitleButton_Click);
            // 
            // subtitlePath
            // 
            this.subtitlePath.AutoSize = true;
            this.subtitlePath.Location = new System.Drawing.Point(200, 81);
            this.subtitlePath.Name = "subtitlePath";
            this.subtitlePath.Size = new System.Drawing.Size(0, 17);
            this.subtitlePath.TabIndex = 5;
            // 
            // loadSubtitleButton
            // 
            this.loadSubtitleButton.Location = new System.Drawing.Point(33, 123);
            this.loadSubtitleButton.Name = "loadSubtitleButton";
            this.loadSubtitleButton.Size = new System.Drawing.Size(136, 23);
            this.loadSubtitleButton.TabIndex = 6;
            this.loadSubtitleButton.Text = "Load Subtitles";
            this.loadSubtitleButton.UseVisualStyleBackColor = true;
            this.loadSubtitleButton.Click += new System.EventHandler(this.loadSubtitleButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.saveEffectButton);
            this.groupBox1.Controls.Add(this.effectsComboBox);
            this.groupBox1.Controls.Add(this.effectLabel);
            this.groupBox1.Controls.Add(this.lineValueLabel);
            this.groupBox1.Controls.Add(this.endTimeValueLabel);
            this.groupBox1.Controls.Add(this.startTimeValueLabel);
            this.groupBox1.Controls.Add(this.linesLabel);
            this.groupBox1.Controls.Add(this.nextTypographButton);
            this.groupBox1.Controls.Add(this.prevTypographButton);
            this.groupBox1.Controls.Add(this.endTimeLable);
            this.groupBox1.Controls.Add(this.startTimeLabel);
            this.groupBox1.Location = new System.Drawing.Point(33, 168);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(677, 291);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Effects management";
            // 
            // effectsComboBox
            // 
            this.effectsComboBox.FormattingEnabled = true;
            this.effectsComboBox.Location = new System.Drawing.Point(114, 255);
            this.effectsComboBox.Name = "effectsComboBox";
            this.effectsComboBox.Size = new System.Drawing.Size(255, 24);
            this.effectsComboBox.TabIndex = 16;
            // 
            // effectLabel
            // 
            this.effectLabel.AutoSize = true;
            this.effectLabel.Location = new System.Drawing.Point(15, 255);
            this.effectLabel.Name = "effectLabel";
            this.effectLabel.Size = new System.Drawing.Size(52, 17);
            this.effectLabel.TabIndex = 15;
            this.effectLabel.Text = "Effect: ";
            // 
            // lineValueLabel
            // 
            this.lineValueLabel.AutoSize = true;
            this.lineValueLabel.Location = new System.Drawing.Point(114, 202);
            this.lineValueLabel.Name = "lineValueLabel";
            this.lineValueLabel.Size = new System.Drawing.Size(0, 17);
            this.lineValueLabel.TabIndex = 14;
            // 
            // endTimeValueLabel
            // 
            this.endTimeValueLabel.AutoSize = true;
            this.endTimeValueLabel.Location = new System.Drawing.Point(114, 159);
            this.endTimeValueLabel.Name = "endTimeValueLabel";
            this.endTimeValueLabel.Size = new System.Drawing.Size(0, 17);
            this.endTimeValueLabel.TabIndex = 13;
            // 
            // startTimeValueLabel
            // 
            this.startTimeValueLabel.AutoSize = true;
            this.startTimeValueLabel.Location = new System.Drawing.Point(114, 120);
            this.startTimeValueLabel.Name = "startTimeValueLabel";
            this.startTimeValueLabel.Size = new System.Drawing.Size(0, 17);
            this.startTimeValueLabel.TabIndex = 12;
            // 
            // linesLabel
            // 
            this.linesLabel.AutoSize = true;
            this.linesLabel.Location = new System.Drawing.Point(15, 202);
            this.linesLabel.Name = "linesLabel";
            this.linesLabel.Size = new System.Drawing.Size(50, 17);
            this.linesLabel.TabIndex = 11;
            this.linesLabel.Text = "Lines: ";
            // 
            // nextTypographButton
            // 
            this.nextTypographButton.Location = new System.Drawing.Point(273, 76);
            this.nextTypographButton.Name = "nextTypographButton";
            this.nextTypographButton.Size = new System.Drawing.Size(75, 23);
            this.nextTypographButton.TabIndex = 10;
            this.nextTypographButton.Text = "Next";
            this.nextTypographButton.UseVisualStyleBackColor = true;
            this.nextTypographButton.Click += new System.EventHandler(this.nextTypographButton_Click);
            // 
            // prevTypographButton
            // 
            this.prevTypographButton.Location = new System.Drawing.Point(170, 76);
            this.prevTypographButton.Name = "prevTypographButton";
            this.prevTypographButton.Size = new System.Drawing.Size(75, 23);
            this.prevTypographButton.TabIndex = 9;
            this.prevTypographButton.Text = "Prev";
            this.prevTypographButton.UseVisualStyleBackColor = true;
            this.prevTypographButton.Click += new System.EventHandler(this.prevTypographButton_Click);
            // 
            // endTimeLable
            // 
            this.endTimeLable.AutoSize = true;
            this.endTimeLable.Location = new System.Drawing.Point(15, 158);
            this.endTimeLable.Name = "endTimeLable";
            this.endTimeLable.Size = new System.Drawing.Size(76, 17);
            this.endTimeLable.TabIndex = 8;
            this.endTimeLable.Text = "End Time: ";
            // 
            // startTimeLabel
            // 
            this.startTimeLabel.AutoSize = true;
            this.startTimeLabel.Location = new System.Drawing.Point(15, 117);
            this.startTimeLabel.Name = "startTimeLabel";
            this.startTimeLabel.Size = new System.Drawing.Size(81, 17);
            this.startTimeLabel.TabIndex = 7;
            this.startTimeLabel.Text = "Start Time: ";
            // 
            // saveEffectButton
            // 
            this.saveEffectButton.Location = new System.Drawing.Point(396, 255);
            this.saveEffectButton.Name = "saveEffectButton";
            this.saveEffectButton.Size = new System.Drawing.Size(98, 23);
            this.saveEffectButton.TabIndex = 17;
            this.saveEffectButton.Text = "Save Effect";
            this.saveEffectButton.UseVisualStyleBackColor = true;
            this.saveEffectButton.Click += new System.EventHandler(this.saveEffectButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 29);
            this.label1.MaximumSize = new System.Drawing.Size(600, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(583, 34);
            this.label1.TabIndex = 18;
            this.label1.Text = "Default effects are applied automatically. To make changes, please browse through" +
    " effects and select the new effect, then save effect.";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 524);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.subtitlePath);
            this.Controls.Add(this.selectSubtitleButton);
            this.Controls.Add(this.audioFilePath);
            this.Controls.Add(this.selectAudioButton);
            this.Controls.Add(this.createVideoButton);
            this.Controls.Add(this.loadSubtitleButton);
            this.Name = "MainForm";
            this.Text = "Typography";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createVideoButton;
        private System.Windows.Forms.Button selectAudioButton;
        private System.Windows.Forms.Label audioFilePath;
        private System.Windows.Forms.Button selectSubtitleButton;
        private System.Windows.Forms.Label subtitlePath;
        private System.Windows.Forms.Button loadSubtitleButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label startTimeLabel;
        private System.Windows.Forms.Label endTimeLable;
        private System.Windows.Forms.Button prevTypographButton;
        private System.Windows.Forms.Button nextTypographButton;
        private System.Windows.Forms.Label linesLabel;
        private System.Windows.Forms.Label startTimeValueLabel;
        private System.Windows.Forms.Label endTimeValueLabel;
        private System.Windows.Forms.Label lineValueLabel;
        private System.Windows.Forms.Label effectLabel;
        private System.Windows.Forms.ComboBox effectsComboBox;
        private System.Windows.Forms.Button saveEffectButton;
        private System.Windows.Forms.Label label1;
    }
}