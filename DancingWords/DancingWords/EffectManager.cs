﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
using VideoWriter;
namespace opentk2
{
    class EffectManager
    {
        private List<Effect> effects = new List<Effect>();
        //private List<Bitmap> generatedFrames = new List<Bitmap>();
        private readonly string _destDir;
        private IVideoWriterAgent videoWriter = null;

        byte[] arr;

        int width;

        int height;

        int last;


        public EffectManager(IVideoWriterAgent videoWriterAgent)
        {
            videoWriter = videoWriterAgent;
            _destDir = CreateOrCleanUpOutputDirectory();
        }

        public void Display(int frame)
        {
            //if( frame == 80 )
            //	Environment.Exit(0);
            foreach (Effect effect in effects)
            {
            	GL.PushMatrix();
                effect.Display(frame, width, height);
                GL.PopMatrix();
                GL.Finish();
            }
            //return;
            SaveSnapshot(frame);
        }

        void SaveSnapshot(int iFrame)
        {
            GCHandle handle = GCHandle.Alloc(arr, GCHandleType.Pinned);
            try
            {
                GL.ReadPixels(0, 0, this.width, this.height, PixelFormat.Bgra, PixelType.UnsignedByte, handle.AddrOfPinnedObject());
                ErrorCode error = GL.GetError();
                if (error == OpenTK.Graphics.OpenGL.ErrorCode.NoError)
                {
                    Bitmap bmp = new Bitmap(this.width, this.height, 4 * this.width, System.Drawing.Imaging.PixelFormat.Format32bppArgb, handle.AddrOfPinnedObject());
                    //string fileName = Path.Combine(_destDir, string.Format("{0}.{1}", iFrame.ToString("000000"), "jpg"));
                    bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    //generatedFrames.Add(bmp);
                    videoWriter.WriteFrame(bmp);
                    //bmp.Save(fileName);
                }
                
            }
            catch
            {
            }
            finally
            {
                handle.Free();
            }
        }

        public void Init()
        {
            foreach (var effect in effects)
            {
                effect.Init();
            }
            videoWriter.Open();
        }

        public void Release()
        {
            videoWriter.Close();
        }

        public void Add(Effect effect)
        {
            effects.Add(effect);
            last = effects.Max(e => e.Range.End) + 1;
        }

        public int Last
        {
            get
            {
                return this.last;
            }
        }

        public void Resize(int width, int height)
        {
            this.width = width;
            this.height = height;
            int sz = height * width * 4;
            arr = new byte[sz];
        }

        private string CreateOrCleanUpOutputDirectory()
        {
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var destDir = Path.Combine(dir, Consts.FolderName);
            if (Directory.Exists(destDir))
            {
                Directory.Delete(destDir, true);
            }
            Directory.CreateDirectory(destDir);
            return destDir;
        }
    }
}

