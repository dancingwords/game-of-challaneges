﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
    class DancingWords : GameWindow
    {
        //QFont heading2;
        QFont mainText;

        readonly EffectManager _manager;

        public DancingWords(EffectManager effectManager, int width, int height)
            : base(width, height)
        {
            _manager = effectManager;
            Size sz = new Size(width, height);
            this.ClientSize = sz;
        }

        ProcessedText processed;

        protected override void OnLoad(EventArgs e)
        {
            //heading2 = QFont.FromQFontFile( "woodenFont.qfont", 1.0f, new QFontLoaderConfiguration(true));
            //QFontBuilderConfiguration builderConfig = new QFontBuilderConfiguration(true);
            //builderConfig.ShadowConfig.blurRadius = 1; //reduce blur radius because font is very small
            //builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit; //best render hint for this font
            //mainText = new QFont("Fonts/times.ttf", 14, builderConfig);
            //processed = mainText.ProcessText("This is a sample file\n and it has to be done in a very systematic way. If not done then you are a dead duck", 400, QFontAlignment.Centre);
            _manager.Init();
            GL.ClearColor(Color.Teal);
        }

        protected override void OnResize(EventArgs e)
        {
            _manager.Resize(this.ClientSize.Width, this.ClientSize.Height);
            GL.Viewport(this.ClientSize);
            var projection = Matrix4.CreateOrthographic(800, 600, -10, 10);
            //var projection = Matrix4.CreatePerspectiveFieldOfView( (float)(Math.PI / 4), Width/(float)Height, 1.0f, 64.0f );
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projection);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            //QFont.Begin();
            //GL.Translate( Width * 0.5f*Math.Sin( cnt * Math.PI / 180.0) + Width*0.5, Height*0.5f, 0f );
            //heading2.Options.Colour.A = ((float)Math.Cos(cnt * Math.PI / 180.0) * 0.5f )+ 0.5f;
            //heading2.Print( "Hello");//, QFontAlignment.Right );
            //mainText.Print(processed );
            //QFont.End();
            _manager.Display(cnt);
            SwapBuffers();
            if (cnt == _manager.Last)
            {
                this.Close();
                _manager.Release();
            }
            cnt++;
        }

        int cnt = 0;
    }
}

