﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	public struct Range
	{
		private readonly int start;

		private readonly int end;

		public int Start {
			get {
				return start;
			}
		}

		public int End {
			get {
				return end;
			}
		}

		public Range(int start, int end)
		{
			this.start = start;
			this.end = end;
		}

		public static float operator /(int i, Range r) {
			return (float)(i - r.start) / (float)(r.end - r.start);
		}

		public bool Contains(int idx)
		{
			return idx >= start && idx <= end;
		}
	}
}

