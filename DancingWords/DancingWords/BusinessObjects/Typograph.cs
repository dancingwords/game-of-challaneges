﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DancingWords.BusinessObjects
{
	class Typograph
    {
        public int SequenceNumber { get; set; }
        public List<string> Lines { get; set; }
        public DateTime StartTime { get; set; }
        public string StartTimeRaw { get; set; }
        public string EndTimeRaw { get; set; }
        public DateTime EndTime { get; set; }
        public opentk2.Effect effect { get; set; }
        
        public Typograph Clone()
        {
        	return this.MemberwiseClone() as Typograph;
        }
        
    }
}
