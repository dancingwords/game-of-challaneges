﻿/*
 * Created by SharpDevelop.
 * User: bhavesh
 * Date: 2/19/2015
 * Time: 2:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
using System.Drawing;

namespace opentk2
{
	/// <summary>
	/// Description of EffectOptions.
	/// </summary>
	public class EffectOptions : ICloneable
	{
		public string text {get; set;}
		public Consts.ScrollDirections Direction {get; set;}
		public Range range {get; set;}
		public Color color {get; set;}

		#region ICloneable implementation

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion
	}
}
