﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	public class Consts
	{
        //---CONFIG PARAMS-----
        internal const int width = 800;
        internal const int height = 600;
        internal const double frameRate = 25.0f;
        internal const string outputVideoFileName = "DancingWords.avi";
        //internal const string inputAudioFileName = @"Play.mp3";
        internal const int rangeStart = 0;
        internal const int rangeEnd = 600;
        internal const string inputText = @"twinkle twinkle little star";//,how i wonder what you are up above the world so high like a diamond in the sky";
        //internal const string srtFilePath = @"..\..\SampleSubtitle.srt";
        internal const int minSecToApplyParticlesEffect = 4;
        //---CONFIG PARAMS-----

		internal const string FolderName = "Images";

		internal const string Extension = "jpg";

		internal const int Height = 480;

		internal const int Width = 640;
		
		internal const string DirectionLeftToRight = "LeftToRigh";
		internal const string DirectionRightToLeft = "RightToLeft";
		internal const string DirectionBottomToTop = "BottomToTop";
		internal const string DirectionTopToBottom = "TopToBottom";

        internal const string SRT_TimeSeparator = "-->";
        internal const int MaxSubstringsInSRT_TimeSeparatorLine = 2;
        internal const string SRT_TimeFormat = "HH:mm:ss,fff";

        internal const float MinFontSize = 2.0f;
        internal const float MaxFontSize = 35.0f;
        internal const int ColorChangeEffectToggleCount = 10;

        public enum ScrollDirections
        {
            DirectionLeftToRight,
            DirectionRightToLeft,
            DirectionBottomToTop,
            DirectionTopToBottom
        }

        public enum Effects
        {
            ScrollTextEffect, 
            ColorChangeEffect,
            FadeOutEffect, 
            RandomLocationWordsEffect,
            ZoomInEffect,
            BounceEffect,
            ZoomOutEffect,
            BounceLetterEffect,
            FadeInEffect,
            TypingEffect
        }
	}
}

