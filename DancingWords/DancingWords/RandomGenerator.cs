﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class RandomGenerator
	{
		private RandomGenerator()
		{
		}

		public readonly Random Random = new Random();

		private static RandomGenerator rg = new RandomGenerator();

		public static RandomGenerator Instance {
			get {
				return rg;
			}
		}
	}
}

