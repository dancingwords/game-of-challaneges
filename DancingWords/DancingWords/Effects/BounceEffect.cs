﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class BounceEffect : Effect
	{
		EffectOptions option;
		QFont mainText;

		ProcessedText processedText;

		string text;

		SizeF sz;
		private bool toggle = true;
		private int lastSplitId = 0;
		private float maxHeight = 0.0f;
		private float tDiff = 0.0f;
		private int index = 0;
		
		public BounceEffect(EffectOptions option) : base(option)
		{
			this.option = option;
		}

		public override void Init()
		{
			var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
			//best render hint for this font
			mainText = new QFont("Fonts/Comfortaa-Regular.ttf", 20, builderConfig);
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / _range;
			double range = 1.0 / (2*option.text.Length);
			var idx = (int)Math.Floor((double)t / range);
			
			if (processedText == null) 
			{
				processedText = mainText.ProcessText(option.text, width / 2, QFontAlignment.Centre);
				sz = mainText.Measure(processedText);
			}
			QFont.Begin();

			//var h = ((height + sz.Height) * t) - sz.Height;
			float baseHeight = height / 2.0f;
			if(maxHeight == 0.0f)
			{
				maxHeight = baseHeight;
			}
			
			string firstLetterText = option.text.Substring(0, 1);
			string remainingLetterText = option.text.Substring(1, option.text.Length - 1);
			//this.mainText.Print(option.text, new Vector2(width / 2.0f, baseHeight));
			
			float tTemp = t - tDiff;
			tDiff = t;
			
			if(toggle)
			{
				var h = (maxHeight + tTemp*index*(sz.Height)) ;//* t;
				this.mainText.Print(option.text, new Vector2(0, h));				
			}
			else
			{
				var h = (baseHeight - tTemp*index*(sz.Height)) ;//* t;
				
				maxHeight = h;
				this.mainText.Print(option.text, new Vector2(0, h));				
			}
			
			index = index + 5;
			if(lastSplitId != idx)
			{
				toggle = !toggle;
				index = 0;
			}
			
			lastSplitId = idx;
			
			QFont.End();
		}
	#endregion
	}
}

