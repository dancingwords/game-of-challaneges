﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class BounceLetterEffect : Effect
	{
		EffectOptions option;
		QFont mainText;

		ProcessedText processedText;
		SizeF sz;
		private bool toggle = true;
        private int charIndex = 0;
		private int lastSplitId = 0;
		private float maxHeight = 0.0f;
		private float tDiff = 0.0f;
		private int index = 0;
		
		public BounceLetterEffect(EffectOptions option) : base(option)
		{
			this.option = option;
		}

		public override void Init()
		{
			var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
			//best render hint for this font
			mainText = new QFont("Fonts/Comfortaa-Regular.ttf", 20, builderConfig);
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / _range;
			double range = 1.0 / (2*option.text.Length);
			var idx = (int)Math.Floor((double)t / range);

            float baseWidth = 0;
			if (processedText == null) 
			{
				processedText = mainText.ProcessText(option.text, width / 2, QFontAlignment.Left);
				sz = mainText.Measure(processedText);
			}
			QFont.Begin();

            
			float baseHeight = height / 2.0f;
			if(maxHeight == 0.0f)
			{
				maxHeight = baseHeight;
			}

            string letterToBounce = string.Empty;
            if (charIndex < option.text.Length)
            {
                letterToBounce = option.text.Substring(charIndex, 1);
            }

            //draw the first parts in which letters are NOT bouncing
            int startIndex = charIndex+1;
            string firstPart = string.Empty;
            string secondPart = string.Empty;
            string remainingLetterText = string.Empty;
            if(startIndex < option.text.Length)
            {
                firstPart = option.text.Substring(0, charIndex);
                secondPart = option.text.Substring(startIndex, option.text.Length - startIndex);
                this.mainText.Print(firstPart, new Vector2(baseWidth, baseHeight));
            }

            //draw the bouncing letter
            ProcessedText pt = mainText.ProcessText(firstPart, width / 2, QFontAlignment.Left);
            var firstPartWidth = mainText.Measure(pt).Width;

			float tTemp = t - tDiff;
			tDiff = t;
			if(toggle)
			{
				var h = (maxHeight + tTemp*index*(sz.Height)) ;//* t;
                this.mainText.Print(letterToBounce, new Vector2(firstPartWidth + baseWidth, h));				
			}
			else
			{
				var h = (baseHeight - tTemp*index*(sz.Height)) ;//* t;
				
				maxHeight = h;
                this.mainText.Print(letterToBounce, new Vector2(firstPartWidth + baseWidth, h));				
			}
			
			index = index + 5;
			if(lastSplitId != idx)
			{
				toggle = !toggle;
				index = 0;

                charIndex = charIndex + 1;
			}
			
			lastSplitId = idx;

            //now draw the remaining second part in which letters are NOT bouncing
            ProcessedText pt1 = mainText.ProcessText(firstPart+letterToBounce, width / 2, QFontAlignment.Left);
            var lastPartStart = mainText.Measure(pt1).Width;
            mainText.Print(secondPart, new Vector2(lastPartStart + baseWidth, baseHeight));

			QFont.End();
		}
	#endregion
	}
}

