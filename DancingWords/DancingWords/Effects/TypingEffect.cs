﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class TypingEffect : Effect
	{
		EffectOptions option;
		QFont mainText;
		private bool showUnderscore = true;
		
		public TypingEffect(EffectOptions options) : base(options)
		{
			option = options;
		}

		public override void Init()
		{
			var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
			//best render hint for this font
			mainText = new QFont("Fonts/Comfortaa-Regular.ttf", 20, builderConfig);
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / this._range;

			double range = 1.0 / option.text.Length;
			var idx = (int)Math.Floor((double)t / range);
			if (idx > option.text.Length)
				return;
			
			string text = string.Empty;
			if(option.text.Length > idx)
			{
				if(showUnderscore) 
				{
					text = option.text.Substring(0, idx) + "_";
				}
				else
				{
					text = option.text.Substring(0, idx);
				}
				showUnderscore = !showUnderscore;//toggle the Underscore for next image
			}
			else
			{
				text = option.text + "_";
			}
			
			QFont.Begin();

            this.mainText.Print(text, new Vector2(0, height / 2.0f));

			QFont.End();

		}
	#endregion
	}
}

