﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class FadeOutEffect : Effect
	{
		QFont font;

		Color color;

		string text;

		public FadeOutEffect(EffectOptions options) : base(options)
		{
			this.text = options.text;
			this.color = options.color;
		}

		public override void Init()
		{
			var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
			//best render hint for this font
			font = new QFont("Fonts/Comfortaa-Regular.ttf", 20, builderConfig);
			font.Options.Colour = new OpenTK.Graphics.Color4(this.color);
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / this._range;
			this.font.Options.Colour.A = (float)Math.Pow(1 - t, 2);
			QFont.Begin();
			font.Print(this.text, QFontAlignment.Centre, new Vector2(width / 2, height / 2));
			QFont.End();
		}
	#endregion
	}
}



