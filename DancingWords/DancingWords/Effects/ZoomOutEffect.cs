﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class ZoomOutEffect : Effect
	{
		EffectOptions option;
		QFont mainText;

		ProcessedText processedText;

		string text;
        SizeF sz;
        private float fontSize = 25;

        public ZoomOutEffect(EffectOptions option)
            : base(option)
		{
			this.option = option;
		}

		public override void Init()
		{

		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / _range;
			QFont.Begin();
			
            var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
            //if (mainText == null)
            //{
            //    mainText = new QFont("Fonts/Comfortaa-Regular.ttf", fontSize, builderConfig);
            //}
            QFont mainText1 = new QFont("Fonts/Comfortaa-Regular.ttf", fontSize, builderConfig);

            //mainText.SetFontSize(fontSize);

            processedText = mainText1.ProcessText(option.text, width / 2, QFontAlignment.Centre);
            sz = mainText1.Measure(processedText);
            mainText1.Print(option.text, new Vector2((width / 2.0f) - (sz.Width / 2), height / 2.0f));

            fontSize = fontSize - t;
            if (fontSize < Consts.MinFontSize)
            {
                fontSize = Consts.MinFontSize;
            }

			QFont.End();
		}
	#endregion
	}
}

