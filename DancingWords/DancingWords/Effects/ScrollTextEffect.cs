﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class ScrollTextEffect : Effect
	{
		EffectOptions option;
		QFont mainText;

		ProcessedText processedText;

		string text;

		SizeF sz;

		public ScrollTextEffect(EffectOptions option) : base(option)
		{
			this.option = option;
		}

		public override void Init()
		{
			var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
			//best render hint for this font
			mainText = new QFont("Fonts/Comfortaa-Regular.ttf", 20, builderConfig);
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / _range;
			if (processedText == null) {
				processedText = mainText.ProcessText(option.text, width / 2, QFontAlignment.Centre);
				sz = mainText.Measure(processedText);
			}
			QFont.Begin();

            if (Consts.ScrollDirections.DirectionLeftToRight == option.Direction)
			{
				var w = ((width + sz.Width) * (1 - t)) - sz.Width;
				this.mainText.Print(this.processedText, new Vector2(w, height / 2.0f));
			}
            else if (Consts.ScrollDirections.DirectionLeftToRight == option.Direction)
			{
				var w = ((width + sz.Width) * t) - sz.Width;
				this.mainText.Print(this.processedText, new Vector2(w, height / 2.0f));
			}
            else if (Consts.ScrollDirections.DirectionTopToBottom == option.Direction)
			{
				var h = ((height + sz.Height) * t) - sz.Height;
				this.mainText.Print(this.processedText, new Vector2(width / 2.0f, h));
			}
			else
			{
				var h = ((height + sz.Height) * (1 - t)) - sz.Height;
				this.mainText.Print(this.processedText, new Vector2(width / 2.0f, h));
			}
			QFont.End();
		}
	#endregion
	}
}

