﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class RandomLocationWordsEffect : Effect
	{
		string[] splits;

		//QFont[] fonts;
		QFont mainText;

		private readonly WordVecMap[] stringSizeMap;

		Color color;

		public RandomLocationWordsEffect(EffectOptions option) : base(option)
		{
			this.color = option.color;
			splits = option.text.Split();
			stringSizeMap = new WordVecMap[splits.Length];
		}

		public override void Init()
		{
			var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
			//best render hint for this font
			mainText = new QFont("Fonts/Comfortaa-Regular.ttf", 40, builderConfig);
			mainText.Options.Colour = new OpenTK.Graphics.Color4(color);
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / this._range;
			// Get the split
			double range = 1.0 / splits.Length;
			var idx = (int)Math.Floor((double)t / range);
			if (idx >= splits.Length)
				return;
			QFont.Begin();
			var str = splits[idx];
			if (stringSizeMap[idx] != null) {
				mainText.Print(str, stringSizeMap[idx].vec);
			}
			else {
				SizeF sz = mainText.Measure(str);
				var random = RandomGenerator.Instance.Random;
				Vector2 vec = new Vector2(random.Next(0, (int)(width - sz.Width)), random.Next(0, (int)(height - sz.Height)));
				stringSizeMap[idx] = new WordVecMap(str, vec);
				mainText.Print(str, vec);
			}
			QFont.End();
		}

		#endregion
		private class WordVecMap
		{
			public string word;

			public Vector2 vec;

			public WordVecMap(string str, Vector2 vec)
			{
				word = str;
				this.vec = vec;
			}
		}
	}
}

