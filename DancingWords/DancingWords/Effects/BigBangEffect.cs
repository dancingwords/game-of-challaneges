﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	class BigBangEffect : Effect
	{
		#region private Members
		const int VERTEX_COUNT = 2500;

		const float SPEED  = 600f;
		
		int vertexBuffer;

		int _prog;

		int _attribPos;

		int _attribVel;

		int _timeLoc;

		int velocityBuffer;

		float[] velocities = new float[VERTEX_COUNT * 2];

		private string vertexShader = @"
		uniform float time;
		uniform vec4 color;
		attribute vec2 inVertex;
		attribute vec2 inVel;
		void main()
		{
			vec2 temp = inVertex;
			temp.x = temp.x + ( inVel.x * time );// + width;
			temp.y = temp.y + ( inVel.y * time );// + height;
			gl_Position = gl_ModelViewProjectionMatrix * vec4(temp,0,1);
			gl_FrontColor = color;
		}
		";

		Color4 _color;
		
		#endregion
		public BigBangEffect(EffectOptions options) : base(options)
		{
			_color = new Color4( options.color );
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			int program = GL.GetInteger( GetPName.CurrentProgram );
		
			GL.LoadIdentity();
			GL.Disable(EnableCap.Texture2D);
			GL.UseProgram( _prog );
			float t = frame/this._range;
			GL.Uniform1(_timeLoc, t);
			GL.Uniform4( GL.GetUniformLocation( _prog, "color" ), _color );
			GL.EnableVertexAttribArray(_attribPos);
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuffer);
			GL.VertexAttribPointer(_attribPos, 2, VertexAttribPointerType.Float, false, 0, 0);
			GL.EnableVertexAttribArray(_attribVel);
			GL.BindBuffer(BufferTarget.ArrayBuffer, velocityBuffer);
			GL.VertexAttribPointer(_attribVel, 2, VertexAttribPointerType.Float, false, 0, 0);
			GL.DrawArrays(PrimitiveType.Points, 0, VERTEX_COUNT);
			
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.UseProgram( program );
			GL.Enable(EnableCap.Texture2D);
		}

		public override void Init()
		{
			Random r = new Random();
			for (int i = 0; i < velocities.Length; i += 2) 
			{
				var rad = (float)(r.NextDouble() * SPEED);
				var ang = r.Next(360) * Math.PI / 180.0;
				velocities[ i ] 	= rad * (float)Math.Cos(ang);
				velocities[ i + 1 ] = rad * (float)Math.Sin(ang);
			}
			vertexBuffer = GL.GenBuffer();
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuffer);
			GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(4 * 2 /*for 2d Vertices*/* VERTEX_COUNT), IntPtr.Zero, BufferUsageHint.StaticDraw);
			velocityBuffer = GL.GenBuffer();
			GL.BindBuffer(BufferTarget.ArrayBuffer, velocityBuffer);
			GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(4 * velocities.Length), velocities, BufferUsageHint.StaticDraw);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			// Create program;
			_prog = AddProgram();
			//GL.UseProgram(_prog);
			_attribPos = GL.GetAttribLocation(_prog, "inVertex");
			_attribVel = GL.GetAttribLocation(_prog, "inVel");
			_timeLoc = GL.GetUniformLocation(_prog, "time");
			GL.PointSize(1.0f);
		}

		private int AddShader(string shaderSrc, ShaderType shaderType)
		{
			int shader = GL.CreateShader(shaderType);
			GL.ShaderSource(shader, shaderSrc);
			GL.CompileShader(shader);
			string log = GL.GetShaderInfoLog(shader);
			Console.WriteLine(log);
			return shader;
		}

		private int AddProgram()
		{
			// Create a vertex shader
			int vs = AddShader(vertexShader, ShaderType.VertexShader);
			var program = GL.CreateProgram();
			GL.AttachShader(program, vs);
			GL.LinkProgram(program);
			var log = GL.GetProgramInfoLog(program);
			Console.WriteLine(log);
			return program;
		}
	#endregion
	}
	
	class RandomPointsEffect : Effect
	{
		public RandomPointsEffect(EffectOptions options) : base(options )
		{
		}
		#region implemented abstract members of Effect
		double[] mat1 = new double[16];
		double[] mat2 = new double[16];
		protected override void Draw(int frame, int width, int height)
		{
			//QFont.Begin();
			GL.Disable(EnableCap.Texture2D);
			GL.LoadIdentity();
			GL.PointSize(2);
			GL.GetDouble(GetPName.ProjectionMatrix,mat1);
			GL.GetDouble(GetPName.ModelviewMatrix,mat2);
			GL.EnableVertexAttribArray(0);
			GL.BindBuffer( BufferTarget.ArrayBuffer, vbo );
			GL.VertexAttribPointer( 0, 3, VertexAttribPointerType.Float, false, 0,0 );
			GL.DrawArrays( PrimitiveType.Points, 0, 1000 );
			GL.BindBuffer( BufferTarget.ArrayBuffer, 0 );
			var code = GL.GetError();
			//QFont.End();
		}
		
		private Vector3[] points;

		int vbo;
		public override void Init()
		{
			points = new Vector3[1000];
			for (int i = 0; i < 1000; i++)
			{
				points[i] = new Vector3(
				200*(float)RandomGenerator.Instance.Random.NextDouble(),
				200*(float)RandomGenerator.Instance.Random.NextDouble(),
				0//200*(float)RandomGenerator.Instance.Random.NextDouble()
				);
			}
			
			vbo = GL.GenBuffer();
			GL.BindBuffer( BufferTarget.ArrayBuffer, vbo );
			GL.BufferData( BufferTarget.ArrayBuffer, new IntPtr(12 * 1000), points, BufferUsageHint.StaticDraw);
			
		}
		
		#endregion
		
		
	}
	
}



