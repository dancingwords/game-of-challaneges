﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
namespace opentk2
{
	abstract class Effect
	{
        public EffectOptions option;

		protected Effect(EffectOptions option)
		{
            this.option = option;
			this._range = option.range;
		}

		public void Display(int frame, int width, int height)
		{
			if (!_range.Contains(frame))
				return;
			Draw(frame, width, height);
		}

		public virtual void Init()
		{
			return;
		}

		public Range Range {
			get {
				return _range;
			}
		}

		protected Range _range;

		protected abstract void Draw(int frame, int width, int height);

		protected void Translate(Vector3 vec)
		{
			GL.Translate(vec);
		}
	}
}

