﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;
using OpenTK.Graphics;
namespace opentk2
{
	class ColorChangeEffect : Effect
	{
		EffectOptions option;
		QFont mainText;

		ProcessedText processedText;

		string text;
        SizeF sz;
        private float fontSize = 25;
        Color4 color;
        int toggleCount = 5;

        public ColorChangeEffect(EffectOptions option)
            : base(option)
		{
			this.option = option;
		}

		public override void Init()
		{
            color = Color4.Blue;
		}

		#region implemented abstract members of Effect
		protected override void Draw(int frame, int width, int height)
		{
			float t = frame / _range;
			QFont.Begin();
			
            var builderConfig = new QFontBuilderConfiguration(true);
			builderConfig.ShadowConfig.blurRadius = 1;
			//reduce blur radius because font is very small
			builderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;
            if (mainText == null)
            {
                mainText = new QFont("Fonts/Comfortaa-Regular.ttf", fontSize, builderConfig);
            }
            if (toggleCount == 0)
            {
                float prevGreen = color.G;
                float prevBlue = color.B;
                float prevRed = color.R;
                color.B = prevGreen;
                color.G = prevRed;
                color.R = prevBlue;

                toggleCount = Consts.ColorChangeEffectToggleCount;
            }
            toggleCount = toggleCount - 1;

            mainText.Options.Colour = color;// color; 
            mainText.Print(option.text, new Vector2(0, height / 2.0f));

			QFont.End();
		}
	#endregion
	}
}

