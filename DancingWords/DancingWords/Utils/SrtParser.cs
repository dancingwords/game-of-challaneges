﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using opentk2;
using System.Globalization;
using DancingWords.BusinessObjects;
using System.Drawing;

namespace DancingWords
{
    class SrtParser
    {
        static public List<Typograph> ParseFile(string srtFilePath)
        {
            List<Typograph> typographies = new List<Typograph>();
            try
            {
                string[] lines = System.IO.File.ReadAllLines(srtFilePath);

                for (int i = 0; i < lines.Length; )
                {
                    string line = GetNextLine(lines, i);
                    if (line.Length > 0)
                    {
                        Typograph typograph = new Typograph();
                        try
                        {
                            typograph.SequenceNumber = Convert.ToInt32(line);
                        }
                        catch
                        {
                            typograph.SequenceNumber = -1;
                        }

                        i = i + 1;
                        line = GetNextLine(lines, i);
                        PopulateTimes(line, typograph);

                        i = i + 1;
                        line = GetNextLine(lines, i);

                        typograph.Lines = new List<string>();
                        while (line.Length > 0)
                        {
                            typograph.Lines.Add(line);

                            i = i + 1;
                            line = GetNextLine(lines, i);
                        }

                        typographies.Add(typograph);
                    }
                    i = i + 1;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Could not parse SRT file" + "Error: " + e.Message);
            }

            //assign the default effects
            AssignDefaultEffects(typographies);

            return typographies;
        }

        static public void PopulateTimes(string line, Typograph typograph)
        {
            if(line.Length > 0)
            {
                string[] stringSeparators = new string[] { Consts.SRT_TimeSeparator };
                string[] times = line.Split(stringSeparators, 
                                            Consts.MaxSubstringsInSRT_TimeSeparatorLine,
                                            StringSplitOptions.RemoveEmptyEntries);
                if(times != null && times.Length == 2)
                {
                    typograph.StartTimeRaw = times[0].TrimStart().TrimEnd();
                    typograph.EndTimeRaw = times[1].TrimStart().TrimEnd();

                    typograph.StartTime = ParseDate(times[0]);
                    typograph.EndTime = ParseDate(times[1]);
                }
            }
        }

        static public DateTime ParseDate(string timeValue)
        {
            string time = timeValue.TrimStart().TrimEnd();
            return DateTime.ParseExact(time,
                                                    Consts.SRT_TimeFormat,
                                                    CultureInfo.InvariantCulture);
        }

        static private string GetNextLine(string[] lines, int index)
        {
            string result = string.Empty;
            if (lines.Length > index)
            {
                result = lines[index];
            }
            return result;
        }

        private static bool isBigBangUsed = false;
        
        static private void AssignDefaultEffects(List<Typograph> typographies)
        {
            Consts.Effects eEffect = 0;
            Consts.Effects maxEnumValue = Enum.GetValues(typeof(Consts.Effects)).Cast<Consts.Effects>().Last();
            
            Consts.ScrollDirections maxScrollEnumValue = Enum.GetValues(typeof(Consts.ScrollDirections)).Cast<Consts.ScrollDirections>().Last();
            Consts.ScrollDirections scrollDirection = maxScrollEnumValue;

            Typograph clone = null;
            
            foreach(Typograph tp in typographies)
            {
                EffectOptions options = new EffectOptions();
                options.Direction = Consts.ScrollDirections.DirectionLeftToRight;

                switch(eEffect)
                {
                    case Consts.Effects.ColorChangeEffect:
                        UpdateEffectOptions(options, tp);
                        tp.effect = new ColorChangeEffect(options);
                        break;
                    case Consts.Effects.ZoomInEffect:
                        UpdateEffectOptions(options, tp);
                        tp.effect = new ZoomInEffect(options);
                        break;
                    case Consts.Effects.ZoomOutEffect:
                        UpdateEffectOptions(options, tp);
                        tp.effect = new ZoomOutEffect(options);
                        break;
                    case Consts.Effects.BounceEffect:
                        UpdateEffectOptions(options, tp);
                        tp.effect = new BounceEffect(options);
                        break;
                    case Consts.Effects.BounceLetterEffect:
                        UpdateEffectOptions(options, tp);
                        tp.effect = new BounceLetterEffect(options);
                        break;
                    case Consts.Effects.FadeInEffect:
                        UpdateEffectOptions(options, tp);
                        options.color = Color.YellowGreen;
                        tp.effect = new FadeInEffect(options);
                        break;
                    case Consts.Effects.FadeOutEffect:
                        UpdateEffectOptions(options, tp);
                        options.color = Color.Red;
                        tp.effect = new FadeOutEffect(options);
                        break;
                    case Consts.Effects.RandomLocationWordsEffect:
                        UpdateEffectOptions(options, tp);
                        options.color = Color.Yellow;
                        tp.effect = new RandomLocationWordsEffect(options);
                        break;
                    case Consts.Effects.ScrollTextEffect:
                        {
                            options.Direction = scrollDirection;
                            scrollDirection = scrollDirection - 1;
                            if (scrollDirection < 0)
                            {
                                scrollDirection = maxScrollEnumValue;
                            }

                            UpdateEffectOptions(options, tp);
                            tp.effect = new ScrollTextEffect(options);
                            break;
                        }
                    case Consts.Effects.TypingEffect:
                        UpdateEffectOptions(options, tp);
                        tp.effect = new TypingEffect(options);
                        break;
                    default:
                        break;
                }

                if (!isBigBangUsed)// && (tp.EndTime - tp.StartTime).TotalSeconds > Consts.minSecToApplyParticlesEffect) 
				{
					clone = tp.Clone();
					EffectOptions obj = options.Clone() as EffectOptions;
					if(obj.color == default(Color))
						obj.color = Color.Aqua;
					clone.effect = new BigBangEffect(obj);
					isBigBangUsed = true;
				}
                eEffect = eEffect + 1;

                //reset the loop after all effects are covered. so it will start from first effect again
                if (eEffect > maxEnumValue)
                {
                    eEffect = 0;
                }
            }
            
            if( clone != null )
            	typographies.Insert( 0, clone );
            
        }

        static public string CreateStringFromList(List<string> lines)
        {
            string result = string.Empty;
            foreach(string s in lines)
            {
                if(result.Length > 0)
                {
                    result = result + "\n";
                }
                result = result + s;
            }

            return result;
        }

        static private void UpdateEffectOptions(EffectOptions options, Typograph tp)
        {
            options.text = CreateStringFromList(tp.Lines);
            options.range = GetRange(tp);
        }

        static private Range GetRange(Typograph tp)
        {
            int start = GetMillisecondsFromTime(tp.StartTime);
            int end = GetMillisecondsFromTime(tp.EndTime);
            double framesPerMillisecons = Consts.frameRate / 1000;
            Range range = new Range((int)(start * framesPerMillisecons), (int)(end * framesPerMillisecons));
            return range;
        }

        static private int GetMillisecondsFromTime(DateTime dt)
        {
            int ms = 0;
            ms = 3600 * 1000 * dt.Hour;
            ms = ms + 60*1000*dt.Minute;
            ms = ms + 1000 * dt.Second;
            ms = ms + dt.Millisecond;

            return ms;
        }
    }
}
