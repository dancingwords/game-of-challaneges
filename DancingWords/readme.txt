Please make sure that all the assemblies present in the References/NativeLibraries are present 
in the final build directory. These assemblies can not be added to the project by references since they are not COM or .NET
libraries rather system libs.

To make these assemblies available to our application we need to either copy these manuall to bin folder or achieve the same using 
post build events.