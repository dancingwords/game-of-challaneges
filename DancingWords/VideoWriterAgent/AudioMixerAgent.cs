﻿using AviFile;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VideoWriter
{
    public interface IAudioMixerAgent
    {
        void AddAudio(string audioFilePath, string videoFilePath);
    }
    public class AudioMixerAgent : IAudioMixerAgent
    {
        AviManager manager = null;
        string tempWaveFilePath = string.Empty;
        public void AddAudio(string audioFilePath, string videoFilePath)
        {
            FileInfo audioFile = new FileInfo(audioFilePath);

            if (audioFile.Extension != "wav")
            {
                tempWaveFilePath = Path.GetFileNameWithoutExtension(audioFilePath) + ".wav";
                using (Mp3FileReader reader = new Mp3FileReader(audioFilePath))
                {
                    WaveFileWriter.CreateWaveFile(tempWaveFilePath, reader);
                }
            }
            string inputAudio = string.IsNullOrEmpty(tempWaveFilePath) ? audioFilePath : tempWaveFilePath;
            try
            {
                manager = new AviManager(videoFilePath, true);
                manager.AddAudioStream(inputAudio, 0);

            }
            catch (Exception ex)
            {
                Console.Write("Could not add audio stream. Error: " + ex.Message);
            }
            finally
            {
                if (manager != null)
                {
                    manager.Close();
                }
            }

        }
    }
}
