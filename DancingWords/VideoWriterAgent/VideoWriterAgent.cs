﻿using AForge.Video.FFMPEG;
using AviFile;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace VideoWriter
{
    public interface IVideoWriterAgent
    {
        void Open();
        void WriteFrame(Bitmap image);
        void Close();
    }
    public class VideoWriterAgent : IVideoWriterAgent
    {
        string outputVideoFile = string.Empty;
        int w, h, frate;
        public VideoWriterAgent(string outputVideoFilePath, int width, int height, int frameRate)
        {
            w = width;
            h = height;
            frate = frameRate;
            outputVideoFile = outputVideoFilePath;
        }
        private VideoFileWriter writer = new VideoFileWriter();
        public void Open()
        {
            writer.Open(outputVideoFile, w, h, frate, VideoCodec.MPEG4, 1000000);
        }
        public void WriteFrame(Bitmap image)
        {
            writer.WriteVideoFrame(image);
        }
        public void Close()
        {
            writer.Close();
        }
    }
}
