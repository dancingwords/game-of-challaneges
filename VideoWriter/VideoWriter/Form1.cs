﻿using AForge.Video.FFMPEG;
using AviFile;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VideoWriter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCreateVideo_Click(object sender, EventArgs e)
        {
            int width = 640;
            int height = 480;
            string[] images = Directory.GetFiles(Properties.Settings.Default.ImagesLocation, "*.jpg");

            VideoFileWriter writer = new VideoFileWriter();
            writer.Open("OutPutVideo.avi", width, height, 25, VideoCodec.MPEG4, 1000000);
            foreach (var img in images)
            {
                Bitmap image = new Bitmap(img);

                writer.WriteVideoFrame(image);
            }

            writer.Close();
            using (Mp3FileReader reader = new Mp3FileReader(@"D:\Personal Projects\VideoWriter\SampleAudio\numb.mp3"))
            {
                WaveFileWriter.CreateWaveFile(@"D:\Personal Projects\VideoWriter\SampleAudio\numb.wav", reader);
                
            }

            AviManager manager = new AviManager("OutPutVideo.avi", true);
            manager.AddAudioStream(@"D:\Personal Projects\VideoWriter\SampleAudio\numb.wav", 0);
            manager.Close();
            
            MessageBox.Show("Video Created Successfully", "Success", MessageBoxButtons.OK);
        }
    }
}
